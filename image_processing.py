import numpy as np
import pywt
import cv2
import math
import os.path
import keras

from keras.models import model_from_json
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler

def transformWavelet(noteGray, mode='haar', level=1):
    noteConversion = noteGray
    noteConversion =  np.float32(noteConversion)
    noteConversion /= 255

    coefficients=pywt.wavedec2(noteConversion, mode, level=level)
    haarCoefficients=list(coefficients)
    haarCoefficients[0] *= 0

    noteHaar=pywt.waverec2(haarCoefficients, mode)
    noteHaar *= 255
    noteHaar =  np.uint8(noteHaar)

    return noteHaar

def calcVariance(noteWavelet):
    variance = np.var(noteWavelet)
    return variance

def calcSkewKurt(noteWavelet):
    height,width = np.shape(noteWavelet)
    rangeX = range(width)
    rangeY = range(height)

    projectionX = np.sum(noteWavelet, axis=1)
    projectionY = np.sum(noteWavelet, axis=0)

    centroidX = np.sum(rangeX*projectionX) / np.sum(projectionX)
    centroidY = np.sum(rangeY*projectionY) / np.sum(projectionY)

    standardX = (rangeX-centroidX)**2
    standardY = (rangeY-centroidY)**2
    standardDeviationX = np.sqrt(np.sum(standardX*projectionX) / np.sum(projectionX))
    standardDeviationY = np.sqrt(np.sum(standardY*projectionY) / np.sum(projectionY))

    skewX = (rangeX-centroidX)**3
    skewY = (rangeY-centroidY)**3
    skewnessX = np.sum(projectionX*skewX) / (np.sum(projectionX)*standardDeviationX**3)
    skewnessY = np.sum(projectionY*skewY) / (np.sum(projectionY)*standardDeviationY**3)

    kurtX = (rangeX-centroidX)**4
    kurtY = (rangeY-centroidY)**4
    kurtosisX = np.sum(projectionX*kurtX) / (np.sum(projectionX) * standardDeviationX**4)
    kurtosisY = np.sum(projectionY*kurtY) / (np.sum(projectionY) * standardDeviationY**4)

    noteSkewness = (skewnessX + skewnessY) / 2
    noteKurtosis = (kurtosisX + kurtosisY) / 2

    return noteSkewness, noteKurtosis

# entropy
def calcEntropy(noteGray):
    frequency = [0]*256
    for row in range(0,400):
        for column in range(0,400):
            position = noteGray[row,column]
            frequency[position] += 1

    dimension = 400*400
    probability = [0] * 256
    for freq in range(0,256):
        probability[freq] = frequency[freq]/dimension

    logarithm = [0] * 256
    for prob in range(0,256):
        if probability[prob] > 0:
            logarithm[prob] = math.log2(probability[prob])

    entropy = [0] * 256
    for log in range(0,256):
        entropy[log] = probability[log] * logarithm[log]

    result = 0
    for iteration in range(0,256):
        result += entropy[iteration]
    result = -result
    return result


print('\n\nCOUNTERFEIT CURRENCY DETECTION USING IMAGE PROCESSING')
print("\n\n> NOTE: image path is the only input required. Enter 'e' to exit.")

print('\n\nInput path for image')
notePath = input('\n> ')

while not os.path.exists(notePath):
    if notePath is not 'e':
        print("\n\n> ERROR: The path doesn't exist. Please Try Again.")
        print('\n\nInput path for image')
        notePath = input('\n> ')
    else:
        exit()

noteRGB = note_rgb = cv2.imread(notePath)

if noteRGB is None:
    print ('\n\nERROR: The selected file is not an image')
    exit()

#noteRESIZE = cv2.resize(noteRGB,(400,400, interpolation = cv2.INTER_LINEAR))
noteRESIZE = cv2.resize(noteRGB,(400,400), interpolation=cv2.INTER_LINEAR)
noteGRAY = cv2.cvtColor(noteRESIZE, cv2.COLOR_BGR2GRAY)
noteWAVELET = transformWavelet(noteGRAY, 'db1', 7)
variance = calcVariance(noteWAVELET)
skewness,kurtosis = calcSkewKurt(noteWAVELET)
entropy = calcEntropy(noteGRAY)
# print
print('\n\nFEATURES\n')
print('\n> variance: ' + str(variance))
print('\n> skewness: ' + str(skewness))
print('\n> kurtosis: ' + str(kurtosis))
print('\n> entropy: ' + str(entropy))
print('\n\n')

# variance = float(input('\n\nvariance\n>'))
# skewness = float(input('\n\nskewness\n>'))
# kurtosis = float(input('\n\nkurtosis\n>'))
# entropy = float(input('\n\nentropy\n>'))

# machine learning integration
sc = StandardScaler()
annInput = [[variance, skewness, kurtosis, entropy]]
annInput = sc.fit_transform(annInput)

# load json and create model
json_file = open('classifier.json', 'r')
loaded_classifierJSON = json_file.read()
json_file.close()
loadedClassifier = model_from_json(loaded_classifierJSON)
# load weights into new model
loadedClassifier.load_weights("classifier.h5")
print("\n\n\n\n\n> ANN model 'classifier' is loaded")

# evaluate loaded model on test data
loadedClassifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
score = loadedClassifier.predict(annInput)
score = (score > 0.5)

print('\n\nRESULT')

if (score == 0):
    print('> The currency note is a counterfeit\n\n')
else:
    print('> The currency note is original\n\n')
