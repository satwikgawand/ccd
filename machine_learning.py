import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import keras
import math

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json

# dataset
dataset = pd.read_csv('data_banknote.csv')

# independent and dependent variables
x = dataset.iloc[:,0:4].values
y = dataset.iloc[:,4:5].values

# split into train and test sets
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.3, random_state = 0)

# scale the independent variables
sc = StandardScaler()
x_train = sc.fit_transform(x_train)
x_test = sc.fit_transform(x_test)

# initialize ANN
classifier = Sequential()

# add layers
classifier.add(Dense(output_dim = 10, init = 'uniform', activation = 'relu', input_dim = 4))
classifier.add(Dense(output_dim = 7, init = 'uniform', activation = 'relu'))
classifier.add(Dense(output_dim = 1, init = 'uniform', activation = 'sigmoid'))

# compile ANN
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# training
classifier.fit(x_train, y_train, batch_size = 5, epochs = 100)

# pred
y_pred = classifier.predict(x_test)
y_pred = (y_pred > 0.5)

# confusion matrix
cm = confusion_matrix(y_test, y_pred)
print('\n\n\n\n\nCONFUSION MATRIX\n')
print(cm)

# accuracy
accuracy = ((cm[0][0] + cm[1][1]) / (cm[0][0] + cm[0][1] + cm[1][0] + cm[1][1])) * 100

print('\n\nACCURACY: ' + str(accuracy) + '%')

# save model to json
classifierJSON = classifier.to_json()
with open("classifier.json", "w") as json_file:
    json_file.write(classifierJSON)

# serialize weights to HDF5
classifier.save_weights("classifier.h5")
print("Artificial Neural Network Classifier saved successfully")